
# from unicodedata import name
from flask import Flask, request, jsonify, make_response
from flask_sqlalchemy import SQLAlchemy
from functools import wraps
import jwt
import uuid
from werkzeug.security import generate_password_hash, check_password_hash
from datetime import date, datetime, timedelta
from sqlalchemy import or_



app = Flask(__name__)

app.config['SECRET_KEY'] = 'thisissecretkey'
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql+psycopg2://postgres:root@localhost/contacts'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

class User(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    public_id = db.Column(db.String(50), unique = True)
    name = db.Column(db.String(100), nullable = True)
    phone = db.Column(db.Integer, nullable = True)
    username = db.Column(db.String(200), unique=True, nullable=False)
    email = db.Column(db.String(150), nullable = False)
    address = db.Column(db.String(200), nullable = True)
    password = db.Column(db.String(250), nullable = False)
    admin = db.Column(db.Boolean)



class Contact(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable = True)
    phone = db.Column(db.String(200), nullable = True)
    email = db.Column(db.String(100), nullable = False)
    address = db.Column(db.String(100), nullable = True)
    country = db.Column(db.Text, nullable = False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))



    @classmethod

    def search_by_user(cls,name,email,phone,user_id):
        
    

        return cls.query.filter(or_(cls.name.ilike(f"%{str(name)}%"),cls.email.ilike(f"%{str(email)}%"),cls.phone.ilike(f"%{str(phone)}%")),cls.user_id==user_id).all()



@app.route('/')
def hello():
    return 'Hello World!'
##################################################
def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None

        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']

        if not token:
            return jsonify({'message' : 'Token is missing!'}), 401

        try:
            # print('---------------------------------------------------------------') 
            data = jwt.decode(token, app.config['SECRET_KEY'],algorithms=["HS256"])
            print(data)
            current_user = User.query.filter_by(public_id=data['public_id']).first()
            user_id = current_user.id
            request.data= eval(str({'user_id':user_id}))
        except Exception as e :
            return jsonify({'message' : e}), 401

        return f(current_user, *args, **kwargs)

    return decorated
#################################################
@app.route('/search',methods=['GET'])
@token_required
def search(current_user):

    data = request.get_json()
    name = data['name']
    phone = data['phone']
    email = data['email']
    contacts = Contact.search_by_user(name,email,phone,user_id=current_user.id)
    output = []
    for contact in contacts:
        contact_data = {}
        
        contact_data['name'] = contact.name
        contact_data['phone'] = contact.phone
        contact_data['email'] = contact.email
        output.append(contact_data)
    return jsonify({'contacts': output})

##############################################################

@app.route('/user', methods=['POST'])
@token_required
def create_user(current_user):
    if not current_user.admin:
        return jsonify({'message' : 'Cannot perform that function!'})
   
    data = request.get_json()

    hashed_password = generate_password_hash(data['password'], method='sha256')

    new_user = User(public_id=str(uuid.uuid4()),username=data['username'], phone=data['phone'],address=data['address'], email=data['email'], name=data['name'], password=hashed_password, admin=False)
    db.session.add(new_user)
    db.session.commit()

    return jsonify({'message' : 'new user created'})


###############################################################

@app.route('/user', methods= ['GET'])
@token_required
def get_all_users(current_user):

    if not current_user.admin:
        return jsonify({'message' : 'Cannot perform that function!'})
    users = User.query.all()
    output = []

    for user in users:
        user_data = {}
        user_data['public_id'] = user.public_id
        user_data['name'] = user.name
        user_data['username'] = user.username
        user_data['email'] = user.email
        user_data['phone'] = user.phone
        user_data['address'] = user.address
        user_data['password'] = user.password
        user_data['admin'] = user.admin
        output.append(user_data)
    return jsonify({'users': output})



###############################################################

@app.route('/login', methods=['POST'])
def login():
    auth = request.authorization
   
    if not auth or not auth.username or not auth.password:
        return make_response('Could not verify', 401, {'WWW-Authenticate' : 'Basic realm="Login required!"'})
    user = User.query.filter_by(username=auth.username).first()
    print(check_password_hash(user.password,auth.password))
    if not user:
        return make_response('Could not verify', 401, {'WWW-Authenticate' : 'Basic realm="Login required!"'})
    if check_password_hash(user.password,auth.password):
        token = jwt.encode({
            'public_id': user.public_id,
            'exp' : datetime.utcnow() + timedelta(days= 30)
        }, app.config['SECRET_KEY'],algorithm="HS256")
  
        return make_response(jsonify({'token' : token}), 201)


    return make_response('Could not verify', 401, {'WWW-Authenticate' : 'Basic realm="Login required!"'})

##############################################################
@app.route('/user/<public_id>',methods=['PUT'])
@token_required
def promote_user(current_user, public_id):
    if not current_user.admin:
        return jsonify({'message' : 'Cannot perform that function!'})

    user = User.query.filter_by(public_id=public_id).first()

    if not user:
        return jsonify({'message' : 'No user found!'})

    user.admin = True
    db.session.commit()

    return jsonify({'message' : 'The user has been promoted!'})


################################################


@app.route('/contact', methods=['POST'])
@token_required
def create_contact(current_user):
    data = request.get_json()

    new_contact = Contact(phone=data['phone'], email=data['email'],address=data['address'],country=data['country'], user_id=current_user.id)
    db.session.add(new_contact)
    db.session.commit()
    return jsonify({'message' : "contact created!"})

######################################################


@app.route('/contact', methods=['GET'])
@token_required
def get_all_contacts(current_user):
    contacts = Contact.query.filter_by(user_id=current_user.id).all()


    output = []

    for contact in contacts:
        contact_data = {}
        contact_data['id'] = contact.id
        contact_data['phone'] = contact.phone
        contact_data['email'] = contact.email
        contact_data['address'] = contact.address
        contact_data['country'] = contact.country
        output.append(contact_data)

    return jsonify({'contacts' : output})










if __name__ == '__main__':
    app.run(debug=True)

